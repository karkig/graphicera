package in.myvocabs.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import in.myvocabs.constants.Constants;
import in.myvocabs.util.AppNotificationManager;
import in.myvocabs.util.AppUtils;
import in.myvocabs.util.Pref;


/**
 * Created by kailash on 30/1/18.
 */

public class NotificationServiceService extends Service {
    @Override
    public int onStartCommand(Intent i, int flags, int startId) {
        super.onStartCommand(i, flags, startId);
        Context context = this;
        String action = i.getAction();
        if (AppNotificationManager.QUOTE_ACTION.equals(action)) {
            AppUtils.showQuoteNotification(context, Pref.readString(context, Constants.PREF_QUOTE_OF_THE_DAY, "Quote"), Pref.readString(context, Constants.PREF_QUOTE_OF_THE_DAY_SAID_BY, "Author"));
        } else if (AppNotificationManager.WORD_ACTION.equals(action)) {
            AppUtils.showWordNotification(context, Pref.readString(context, Constants.PREF_WORD_OF_THE_DAY, "Word"), Pref.readString(context, Constants.PREF_WORD_OF_THE_DAY_MEANING, "Meaning"));
        }
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
    }

}
