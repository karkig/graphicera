package in.myvocabs.services;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import in.myvocabs.BuildConfig;
import in.myvocabs.constants.Constants;
import in.myvocabs.db.Dictionary;
import in.myvocabs.db.DictionaryDB;
import in.myvocabs.db.dao.DictionaryDao;
import in.myvocabs.util.AppLog;
import in.myvocabs.util.FileManager;
import in.myvocabs.util.Pref;

public class DbCreateIntentService extends IntentService {
    public DbCreateIntentService() {
        super("DbCreateIntentService");
    }

    public static void startCreatingDictionary(Context context) {
        boolean isDictionaryCreated = Pref.readBoolean(context, Constants.PREF_IS_DICTIONARY_CREATED);
        if (!isDictionaryCreated) {
            Intent intent = new Intent(context, DbCreateIntentService.class);
            context.startService(intent);
        }
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        try {
            String fileName = "";
            if (BuildConfig.DEBUG) {
                fileName = FileManager.TEMP_FILE;
            } else {
                fileName = FileManager.WORDS_FILE;
            }
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(getAssets().open(fileName)));
            String word = "";
            while ((word = reader.readLine()) != null && word.length() != 0) {
                DictionaryDao dao = DictionaryDB.getAppDatabase(getApplicationContext()).getDictionaryDao();
                Dictionary dictionary = new Dictionary();
                dictionary.setWord(word);
                dao.insert(dictionary);
            }
            Pref.writeBoolean(getApplicationContext(), Constants.PREF_IS_DICTIONARY_CREATED, true);
            AppLog.e("--------------  db created");
        } catch (IOException e) {
            AppLog.e("----------------- " + e.getMessage());
            e.printStackTrace();
        }
    }
}
