package in.myvocabs.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.MatrixCursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jakewharton.rxbinding.view.RxView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.myvocabs.BuildConfig;
import in.myvocabs.R;
import in.myvocabs.activity.search.SearchableActivity;
import in.myvocabs.adapters.search.DictionarySearchAdapter;
import in.myvocabs.constants.Constants;
import in.myvocabs.controller.ModelCreator;
import in.myvocabs.db.Dictionary;
import in.myvocabs.db.DictionaryDB;
import in.myvocabs.model.BaseResponse;
import in.myvocabs.model.quote.QuoteResponse;
import in.myvocabs.model.searchlist.CustomSearchModel;
import in.myvocabs.model.version.VersionResponse;
import in.myvocabs.model.weather.WeatherResponse;
import in.myvocabs.model.word.Data;
import in.myvocabs.model.word.WordResponse;
import in.myvocabs.model.youtube.VideoSearchResponse;
import in.myvocabs.network.APIService;
import in.myvocabs.network.RequestResponses;
import in.myvocabs.services.DbCreateIntentService;
import in.myvocabs.services.GPSTracker;
import in.myvocabs.util.AppLog;
import in.myvocabs.util.AppNotificationManager;
import in.myvocabs.util.AppUtils;
import in.myvocabs.util.Helper;
import in.myvocabs.util.Pref;
import in.myvocabs.util.Validator;
import retrofit2.Response;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

import static in.myvocabs.config.ProjectConfig.NUMBER_OF_YOUTUBE_ITEM;

public class HomeActivity extends AppCompatActivity {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.nav_view)
    NavigationView navView;
    @BindView(R.id.menuIcon)
    ImageView menuIcon;

    @BindView(R.id.drawerLayout)
    DrawerLayout drawerLayout;
    @BindView(R.id.playIconText)
    TextView playIconText;

    @BindView(R.id.timeOfIndiaLayout)
    LinearLayout timeOfIndiaLayout;

    @BindView(R.id.timeOfIndiaLayoutIcon)
    LinearLayout timeOfIndiaLayoutIcon;

    @BindView(R.id.BbcLayout)
    LinearLayout BbcLayout;
    @BindView(R.id.BbcLayoutIcon)
    LinearLayout BbcLayoutIcon;

    @BindView(R.id.techNewsLayout)
    LinearLayout techNewsLayout;

    @BindView(R.id.techNewsLayoutIcon)
    LinearLayout techNewsLayoutIcon;

    @BindView(R.id.tempStatus)
    TextView tempStatus;
    @BindView(R.id.tempratureIcon)
    TextView tempratureIcon;
    @BindView(R.id.tempratureDegreeValue)
    TextView tempratureDegreeValue;

    @BindView(R.id.location)
    TextView location;
    @BindView(R.id.minTemp)
    TextView minTemp;
    @BindView(R.id.maxTemp)
    TextView maxTemp;
    @BindView(R.id.word)
    TextView word;
    @BindView(R.id.meaningOfWord)
    TextView meaningOfWord;
    @BindView(R.id.quoteData)
    TextView quoteData;
    @BindView(R.id.authorName)
    TextView authorName;
    @BindView(R.id.debateCard)
    CardView debateCard;
    @BindView(R.id.vocabsTestCard)
    CardView vocabsTestCard;
    @BindView(R.id.version)
    TextView version;
    @BindView(R.id.quoteLikeThum)
    TextView quoteLikeThum;
    @BindView(R.id.wordLikeBtn)
    LinearLayout wordLikeBtn;
    @BindView(R.id.quoteLikeBtn)
    LinearLayout quoteLikeBtn;
    @BindView(R.id.exampleBtn)
    LinearLayout exampleBtn;
    @BindView(R.id.wordShareBtn)
    LinearLayout wordShareBtn;
    @BindView(R.id.likeCount)
    TextView wordLikeCount;
    @BindView(R.id.quoteLikeCount)
    TextView quoteLikeCount;
    @BindView(R.id.quoteShareBtn)
    LinearLayout quoteShareBtn;
    @BindView(R.id.wordLikeThumb)
    TextView wordLikeThumb;
    @BindView(R.id.quoteContribute)
    LinearLayout quoteContribute;

    @BindView(R.id.feedback)
    FloatingActionButton feedback;
    @BindView(R.id.shareApp)
    TextView shareApp;
    @BindView(R.id.home_menu)
    TextView home_menu;

    @BindView(R.id.searchDictionary)
    SearchView searchDictionary;

    private String defaultServiewText = "Dictionary";

    HomeActivity context;

    GPSTracker mGps;    // mGps coordinate provider
    double mLatitude;   // latitude holder
    double mLongitude;

    String tagString = "latest english debates";
    private ArrayList<CustomSearchModel> data;
    private List<Dictionary> wordsList = new ArrayList<>();
    private DictionarySearchAdapter searchAdapter;
    private String[] columnsSearch = new String[]{"_id", "word"};
    private MatrixCursor cursorSearch = new MatrixCursor(columnsSearch);
    private static final int READ_STORAGE_PERMISSION = 4000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        context = this;
        initView();
        initData();
        clickListeners();
        setUpAnimation();
        DbCreateIntentService.startCreatingDictionary(getBaseContext());


        /**********************************/
        if (Helper.requestStoragePermission(this, READ_STORAGE_PERMISSION)) {
        } else {

        }
        /*********************************************************/
    }

    @Override
    protected void onResume() {
        super.onResume();
        setSearchView();
        defaultServiewText = "Dictionary";
    }

    void setSearchView() {
        searchDictionary.setFocusable(false);
        searchDictionary.clearFocus();
        searchDictionary.setQueryHint(defaultServiewText);
        searchDictionary.setQuery("", false);
    }

    private void prepareSearchItems(Dictionary newWords[]) {
        try {
            wordsList.clear();
            wordsList.addAll(Arrays.asList(newWords));
            cursorSearch = new MatrixCursor(columnsSearch);

            for (int i = 0; i < wordsList.size(); i++) {
                cursorSearch.addRow(new Object[]{0, wordsList.get(i).getWord()});
            }
            if (searchAdapter == null) {
                searchAdapter = new DictionarySearchAdapter(context, cursorSearch, wordsList);
            } else {

                searchAdapter.notifyDataSetChanged();
            }

            searchDictionary.setOnSuggestionListener(new SearchView.OnSuggestionListener() {
                @Override
                public boolean onSuggestionSelect(int position) {
                    return false;
                }

                @Override
                public boolean onSuggestionClick(int position) {
                    AppLog.e("------------  onSuggestionClick" + position);
                    defaultServiewText = wordsList.get(position).getWord();
                    searchDictionary.setQuery(defaultServiewText, true);
                    return true;
                }
            });
            searchDictionary.setSuggestionsAdapter(searchAdapter);
        } catch (Exception e) {
            AppLog.e("----------------- " + e.getMessage());
        }

    }

    @Override
    protected void onStop() {
        super.onStop();
        defaultServiewText = "Dictionary";
        setSearchView();
    }

    public void setupSearch() {

        searchDictionary.clearFocus();
        playIconText.requestFocus(); // just removing the rquestfocus form searchview
        searchDictionary.setQueryHint("Dictionary");
        searchDictionary.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                defaultServiewText = "Dictionary";
                setSearchView();
                return false;
            }
        });
        searchDictionary.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                defaultServiewText = query;
                Intent intent = new Intent(context, SearchableActivity.class);
                intent.putExtra(Constants.INTENT_SEARCH_WORD, query);
                startActivity(intent);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                Dictionary words[] = DictionaryDB.getAppDatabase(context).getDictionaryDao().findByWord("%" + newText + "%");
                prepareSearchItems(words);
                return false;
            }
        });

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == READ_STORAGE_PERMISSION) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            } else {
            }

        } else {
        }
        checkRequestIsDeniedOrNot();
    }

    private void checkRequestIsDeniedOrNot() {
        getLonLat();
    }

    private void checkForPermission() {
        if (ContextCompat.checkSelfPermission(context,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(
                        context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(HomeActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        } else {
            getLonLat();
        }
    }

    private void initData() {
        getWordOfTheDay();
        getQuoteOfTheDay();
        checkAppVer();
        getVideosFromYoutubeServer(tagString);
        AppNotificationManager.setAlarm(context);
    }


    /**
     * Long and Lat provider
     */
    private void getLonLat() {
        mGps = new GPSTracker(HomeActivity.this);
        // Check if GPS enabled
        if (mGps.canGetLocation()) {
            mLatitude = mGps.getLatitude();
            mLongitude = mGps.getLongitude();
            getWeatherConditions(mLatitude, mLongitude);
        } else {
            mGps.showSettingsAlert();
        }
    }

    public void getWeatherConditions(Double mLatitude, Double mLongitude) {
        HashMap<String, String> params = new HashMap<>();
        params.put("lat", mLatitude + "");
        params.put("lon", mLongitude + "");
        params.put("appid", Constants.OPEN_WEATHER_MAP_API_KEY);

        APIService requestUrl = RequestResponses.getObservableServiceRequest(context, 0);
        requestUrl.getWeatherData(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Response<WeatherResponse>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        AppLog.e("-------- " + throwable.getMessage());
                    }

                    @Override
                    public void onNext(Response<WeatherResponse> objectResponse) {

                        WeatherResponse data = objectResponse.body();
                        if (data != null) {
                            if (Validator.isValidString(data.getName())) {  //setting city name
                                location.setText("Location : " + data.getName());
                            }
                            if (data.getWeather() != null) {
                                if (data.getWeather().size() > 0) {
                                    String status = data.getWeather().get(0).getMain();
                                    double temp = data.getMain().getTemp();
                                    double minTemprature = data.getMain().getTempMin();
                                    double maxTemprature = data.getMain().getTempMax();

                                    temp = temp - 273.15;   //  celsius = kelvin -273.15
                                    minTemprature = minTemprature - 273.15;
                                    maxTemprature = maxTemprature - 273.15;
                                    tempStatus.setText("" + status);
                                    minTemp.setText("min : " + (int) minTemprature);
                                    maxTemp.setText("max : " + (int) maxTemprature);

                                    tempratureDegreeValue.setText(" " + (int) temp);
                                    if ("Haze".equalsIgnoreCase(status)) {
                                        tempratureIcon.setText(R.string.haze_day_icon);
                                    } else if ("Fog".equalsIgnoreCase(status) || "Mist".equalsIgnoreCase(status)) {
                                        tempratureIcon.setText(R.string.fog_icon);
                                    } else if ("Sunny Day".equalsIgnoreCase(status)) {
                                        tempratureIcon.setText(R.string.sunny_day_icon);
                                    } else {
                                        tempratureIcon.setText(R.string.haze_day_icon);
                                    }
                                }
                            }

                        }
                    }
                });
    }

    public void getQuoteOfTheDay() {
        APIService requestUrl = RequestResponses.getObservableServiceRequest(context, 0);
        requestUrl.getQuoteOfTheDay()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Response<QuoteResponse>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        AppLog.e("-------- " + throwable.getMessage());
                    }

                    @Override
                    public void onNext(Response<QuoteResponse> objectResponse) {
                        QuoteResponse data = objectResponse.body();
                        if (data != null && data.status) {
                            String author = data.getData().getAuthor() != null ? data.getData().getAuthor() : "";
                            String quote = data.getData().getQuote() != null ? data.getData().getQuote() : "";
                            int likes = data.getData().getLikes() != null ? data.getData().getLikes() : 0;
                            quoteData.setText(quote);
                            authorName.setText("-by  " + author + "");
                            quoteLikeCount.setText("" + likes);
                            Pref.writeString(context, Constants.PREF_QUOTE_OF_THE_DAY, data.getData().getQuote() + "");
                            Pref.writeString(context, Constants.PREF_QUOTE_OF_THE_DAY_SAID_BY, data.getData().getAuthor() + "");
                        } else {

                        }
                    }
                });
    }


    public void getWordOfTheDay() {
        APIService requestUrl = RequestResponses.getObservableServiceRequest(context, 0);
        requestUrl.getWordOfTheDay()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Response<WordResponse>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        AppLog.e("-------- getWordOfTheDay " + throwable.getMessage());
                    }

                    @Override
                    public void onNext(Response<WordResponse> objectResponse) {
                        WordResponse data = objectResponse.body();
                        if (data != null && data.status && data.getData() != null) {
                            int likes = data.getData().getLikes() != null ? data.getData().getLikes() : 0;
                            wordLikeCount.setText("" + likes);
                            word.setText(data.getData().getWord() + "");
                            word.setTag(data.getData());
                            String wordMeaningIs = data.getData().getMeaning();
                            wordMeaningIs = wordMeaningIs.replace("\n", "");
                            meaningOfWord.setText("meaning : " + wordMeaningIs + "");
                            Pref.writeString(context, Constants.PREF_WORD_OF_THE_DAY, data.getData().getWord() + "");
                            Pref.writeString(context, Constants.PREF_WORD_OF_THE_DAY_MEANING, data.getData().getMeaning() + "");

                        } else {
                        }
                    }
                });
    }

    public void updateWordLike(boolean isLiked) {
        HashMap<String, Boolean> map = new HashMap<>();
        map.put("like", isLiked);
        APIService requestUrl = RequestResponses.getObservableServiceRequest(context, 0);
        requestUrl.updateWordLike(map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Response<BaseResponse>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable throwable) {
                    }

                    @Override
                    public void onNext(Response<BaseResponse> objectResponse) {
                        BaseResponse data = objectResponse.body();
                        if (data != null && data.status) {

                            int like = Integer.parseInt(wordLikeCount.getText().toString());
                            wordLikeCount.setText(isLiked ? "" + (like + 1) : "" + (like - 1));


                            // animate like buttton and update
                            AppUtils.setLikeBtnAnimation(context, wordLikeThumb);
                        } else {

                        }
                    }
                });
    }

    public void updateQuoteLike(boolean isLiked) {
        HashMap<String, Boolean> map = new HashMap<>();
        map.put("like", isLiked);
        APIService requestUrl = RequestResponses.getObservableServiceRequest(context, 0);
        requestUrl.updateQuoteLike(map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Response<BaseResponse>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        AppLog.e("-------- " + throwable.getMessage());
                    }

                    @Override
                    public void onNext(Response<BaseResponse> objectResponse) {
                        BaseResponse data = objectResponse.body();
                        if (data != null && data.status) {
                            int like = Integer.parseInt(quoteLikeCount.getText().toString());
                            quoteLikeCount.setText(isLiked ? "" + (like + 1) : "" + (like - 1));

                            // animate like buttton and update
                            AppUtils.setLikeBtnAnimation(context, quoteLikeThum);
                        } else {

                        }
                    }
                });
    }


    private void checkAppVer() {
        try {

            HashMap<String, String> params = new HashMap<>(2);
            params.put("p_frm", Constants.PLATFORM);
            APIService requestURLs = RequestResponses.getObservableServiceRequest(context, 0);
            requestURLs.updateAppVersion(params)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<Response<VersionResponse>>() {
                        @Override
                        public void onCompleted() {

                        }

                        @Override
                        public void onError(Throwable e) {

                        }

                        @Override
                        public void onNext(retrofit2.Response<VersionResponse> appResponse) {
                            if (appResponse.body() != null && appResponse.body().getData() != null) {
                                in.myvocabs.model.version.Data ver = appResponse.body().getData();
                                AppUtils.showAppUpdateDialog(ver, context);

                            }


                        }
                    });
        } catch (Exception e) {
        }
    }

    private void setUpAnimation() {
        Animation rotation = AnimationUtils.loadAnimation(context, R.anim.rotate);
        rotation.setFillAfter(true);
        feedback.startAnimation(rotation);
    }

    private void clickListeners() {

      /*  RxView.clicks(searchDictionary).subscribe(aVoid -> {
            AppLog.e("---  on Search ");
            //startActivity(new Intent(context, SearchableActivity.class));
            onSearchRequested();
        });
*/

        RxView.clicks(exampleBtn).subscribe(aVoid -> {
            Intent intent = new Intent(context, WordDetailActivity.class);

            intent.putExtra(Constants.INTENT_WORD_DATA, (Data) word.getTag());
            startActivity(intent);
        });
        RxView.clicks(shareApp).subscribe(aVoid -> {

            AppUtils.shareQuote(context);
            drawerLayout.closeDrawer(GravityCompat.START);

        });
        RxView.clicks(home_menu).subscribe(aVoid -> {
            drawerLayout.closeDrawer(GravityCompat.START);
            openNewTab(Constants.OPEN_SITE);


        });

        RxView.clicks(debateCard).subscribe(aVoid -> {
            Intent intent = new Intent(context, DebatesListActivity.class);
            intent.putParcelableArrayListExtra(Constants.INTENT_YOUTUBE_LIST_DATA, data);
            startActivity(intent);
        });
        RxView.clicks(vocabsTestCard).subscribe(aVoid -> {
            AppUtils.showToast("Coming soon...");
        });


        RxView.clicks(menuIcon).subscribe(aVoid -> {
            if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                drawerLayout.closeDrawer(GravityCompat.START);
            } else {
                drawerLayout.openDrawer(Gravity.START);
            }
        });

        RxView.clicks(techNewsLayout).subscribe(new Action1<Void>() {
            @Override
            public void call(Void aVoid) {
                openNewTab(Constants.TECHNICAL_WORLD_NEWS);
            }
        });

        RxView.clicks(techNewsLayoutIcon).subscribe(new Action1<Void>() {
            @Override
            public void call(Void aVoid) {
                openNewTab(Constants.TECHNICAL_WORLD_NEWS);
            }
        });
        RxView.clicks(BbcLayoutIcon).subscribe(new Action1<Void>() {
            @Override
            public void call(Void aVoid) {
                openNewTab(Constants.BBC_NEWS);
            }
        });
        RxView.clicks(BbcLayout).subscribe(new Action1<Void>() {
            @Override
            public void call(Void aVoid) {
                openNewTab(Constants.BBC_NEWS);
            }
        });
        RxView.clicks(timeOfIndiaLayoutIcon).subscribe(new Action1<Void>() {
            @Override
            public void call(Void aVoid) {
                openNewTab(Constants.TIMES_OF_INDIA_NEWS);
            }
        });
        RxView.clicks(timeOfIndiaLayout).subscribe(new Action1<Void>() {
            @Override
            public void call(Void aVoid) {
                openNewTab(Constants.TIMES_OF_INDIA_NEWS);
            }
        });
        RxView.clicks(wordShareBtn).subscribe(new Action1<Void>() {
            @Override
            public void call(Void aVoid) {
                AppUtils.shareWord(context, word.getText().toString(), meaningOfWord.getText().toString());
            }
        });

        RxView.clicks(quoteShareBtn).subscribe(aVoid -> AppUtils.shareQuote(context, quoteData.getText().toString(), authorName.getText().toString()));

        RxView.clicks(quoteLikeBtn).subscribe(aVoid -> {

            Boolean tag = Boolean.parseBoolean(quoteLikeBtn.getTag().toString());
            if (tag != null) {
                updateQuoteLike(!tag);
                quoteLikeBtn.setTag(!tag);
            }
        });


        RxView.clicks(quoteContribute).subscribe(aVoid -> {

            startActivity(new Intent(context, ContributeQuoteActivity.class));

        });
        RxView.clicks(feedback).subscribe(aVoid -> {

            //new ContributeQuoteDialogFragment(context).show();
            startActivity(new Intent(context, FeedbackFormActivity.class));

        });

        RxView.clicks(wordLikeBtn).subscribe(aVoid -> {

            Boolean tag = Boolean.parseBoolean(wordLikeBtn.getTag().toString());
            if (tag != null) {
                updateWordLike(!tag);
                wordLikeBtn.setTag(!tag);
            }
        });

    }

    private void openNewTab(int channel) {
        Intent intent = new Intent(context, WebViewActivity.class);
        intent.setAction(Intent.ACTION_VIEW);
        if (Constants.TIMES_OF_INDIA_NEWS == channel) {

            intent.setData(Uri.parse("https://timesofindia.indiatimes.com/india"));
        } else if (Constants.BBC_NEWS == channel) {
            intent.setData(Uri.parse("http://www.bbc.com/news"));

        } else if (Constants.TECHNICAL_WORLD_NEWS == channel) {
            intent.setData(Uri.parse("https://www.technewsworld.com/"));
        } else if (Constants.OPEN_SITE == channel) {
            intent.setData(Uri.parse("http://www.geu.ac.in/"));
        }
        startActivity(intent);
    }


    private void initView() {
        //Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        // setSupportActionBar(toolbar);

        String verName = BuildConfig.VERSION_NAME;
        version.setText("Version : " + verName);
        setupSearch();
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    /************************************************   Ad Work ******************************************/

    private void getVideosFromYoutubeServer(String searchKey) {
        HashMap<String, String> params = new HashMap<>();
        params.put("part", Constants.SNIPPET_PARAMS);
        params.put("key", Constants.API_KEY);
        params.put("maxResults", NUMBER_OF_YOUTUBE_ITEM + "");
        params.put("safeSearch", "strict");
        params.put("videoDuration", "medium");
        params.put("type", "video");
        params.put("q", searchKey + " debate");

        APIService requestUrl = RequestResponses.getObservableServiceRequest(context, 0);
        requestUrl.getVideoListFromYoutube(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Response<VideoSearchResponse>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        AppLog.e("-------- " + throwable.getMessage());

                    }

                    @Override
                    public void onNext(Response<VideoSearchResponse> objectResponse) {
                        VideoSearchResponse videoSearchResponse = objectResponse.body();
                        data = ModelCreator.createListModel(videoSearchResponse);
                    }
                });
    }
}
