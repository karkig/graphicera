package in.myvocabs.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.formats.NativeAd;
import com.jakewharton.rxbinding.view.RxView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.myvocabs.R;
import in.myvocabs.adapters.YouTubeListAdapter;
import in.myvocabs.config.AddMobConfig;
import in.myvocabs.constants.Constants;
import in.myvocabs.controller.ModelCreator;
import in.myvocabs.model.debate.Data;
import in.myvocabs.model.debate.DebateTagResponse;
import in.myvocabs.model.debate.Tag;
import in.myvocabs.model.searchlist.CustomSearchModel;
import in.myvocabs.model.youtube.VideoSearchResponse;
import in.myvocabs.network.APIService;
import in.myvocabs.network.RequestResponses;
import in.myvocabs.util.AppLog;
import in.myvocabs.util.AppUtils;
import retrofit2.Response;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static in.myvocabs.config.ProjectConfig.NUMBER_OF_ADS;
import static in.myvocabs.config.ProjectConfig.NUMBER_OF_YOUTUBE_ITEM;

public class DebatesListActivity extends AppCompatActivity {
    Context context;
    @BindView(R.id.videoRecyclerList)
    RecyclerView videoRecyclerList;
    @BindView(R.id.etKeyword)
    SearchView etKeyword;
    @BindView(R.id.backIcon)
    TextView backIcon;
    @BindView(R.id.progress)
    ProgressBar progressBar;
    @BindView(R.id.searchBtn)
    TextView searchBtn;

    @BindView(R.id.tagContainer)
    LinearLayout tagContainer;

    private List<Object> mRecyclerViewItems = new ArrayList<>();
    // The number of native ads to load.
    // List of MenuItems and native ads that populate the RecyclerView.
    // private List<Object> mRecyclerViewItems = new ArrayList<>();

    // List of native ads that have been successfully loaded.
    private List<NativeAd> mNativeAds = new ArrayList<>();
    private YouTubeListAdapter adapter;
    List<Tag> tagsList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_debate_list);
        ButterKnife.bind(this);
        context = this;
        initView();
        clickListener();
        getDebateTag();
        if (getIntent() != null) {
            ArrayList<CustomSearchModel> data = getIntent().getParcelableArrayListExtra(Constants.INTENT_YOUTUBE_LIST_DATA);
            if (null != data) {
                mRecyclerViewItems.clear();
                mRecyclerViewItems.addAll(data);
                loadDataToList();
                // loadNativeAd();   once native advance ads available the enable this.
            }

        }
    }

    public void createTagsLayout() {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        tagContainer.removeAllViews();
        for (Tag t : tagsList) {
            LinearLayout tagView = (LinearLayout) inflater.inflate(R.layout.tag_layout_item, null);

            TextView text = (TextView) tagView.findViewById(R.id.tag);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(10, 10, 10, 10);//pass int values for left,top,right,bottom
            text.setLayoutParams(params);

            text.setText(t.getTg() + "");
            tagView.setOnClickListener(v -> {
                callSearchApi(text.getText().toString() + "");
                AppLog.e("----");
            });
            tagContainer.addView(tagView);
        }
    }

    private void clickListener() {

        etKeyword.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                getVideosFromYoutubeServer(etKeyword.getQuery().toString());
                videoRecyclerList.requestFocus();
                etKeyword.clearFocus();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return true;
            }
        });
        RxView.clicks(searchBtn).subscribe(aVoid -> {
            getVideosFromYoutubeServer(etKeyword.getQuery().toString());
            etKeyword.clearFocus();
        });
        etKeyword.setOnSearchClickListener(v -> {
            etKeyword.clearFocus();
            getVideosFromYoutubeServer(etKeyword.getQuery().toString());
        });
        RxView.clicks(backIcon).subscribe(aVoid -> onBackPressed());
    }

    private void callSearchApi(String tag) {
        getVideosFromYoutubeServer(tag);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void getDebateTag() {
        APIService requestUrl = RequestResponses.getObservableServiceRequest(context, 0);
        requestUrl.getDebateTags()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Response<DebateTagResponse>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        AppLog.e("-------- " + throwable.getMessage());
                    }

                    @Override
                    public void onNext(Response<DebateTagResponse> objectResponse) {
                        DebateTagResponse data = objectResponse.body();
                        if (data != null && data.status) {
                            Data tags = data.getData();
                            tagsList.clear();
                            tagsList.addAll(tags.getTags());
                            createTagsLayout();
                        } else {

                        }
                    }
                });
    }

    private void updateAdapter(ArrayList<CustomSearchModel> listModel) {
        mRecyclerViewItems.clear();
        mRecyclerViewItems.addAll(listModel);
        loadDataToList();
    }

    private void getVideosFromYoutubeServer(String searchKey) {
        //  loadNativeAd(); // loading new ads
        progressBar.setVisibility(View.VISIBLE);
        HashMap<String, String> params = new HashMap<>();
        params.put("part", Constants.SNIPPET_PARAMS);
        params.put("key", Constants.API_KEY);
        params.put("maxResults", NUMBER_OF_YOUTUBE_ITEM + "");
        params.put("safeSearch", "strict");
        params.put("videoDuration", "medium");
        params.put("type", "video");
        params.put("q", searchKey + " debate");

        APIService requestUrl = RequestResponses.getObservableServiceRequest(context, 0);
        requestUrl.getVideoListFromYoutube(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Response<VideoSearchResponse>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        AppLog.e("-------- " + throwable.getMessage());
                        progressBar.setVisibility(View.GONE);
                        videoRecyclerList.setVisibility(View.GONE);
                    }

                    @Override
                    public void onNext(Response<VideoSearchResponse> objectResponse) {
                        VideoSearchResponse videoSearchResponse = objectResponse.body();
                        updateAdapter(ModelCreator.createListModel(videoSearchResponse));
                    }
                });
    }

    private void initView() {
        videoRecyclerList.setLayoutManager(new LinearLayoutManager(context));
        AppUtils.setListAnimation(context, videoRecyclerList);
        String[] tagArray = context.getResources().getStringArray(R.array.debate_tag_list);
        tagsList.clear();
        for (String s : tagArray) {
            Tag tag = new Tag();
            tag.setEnable(true);
            tag.setPriority(1);
            tag.setTg(s);
            tagsList.add(tag);
        }
        createTagsLayout();
    }

    /************************************************************************************************/

    private void loadNativeAd() {
        mNativeAds.clear();
        loadNativeAd(0);
    }

    private void insertAdsInMenuItems() {
        if (mNativeAds.size() <= 0) {
            return;
        }

        int offset = (mRecyclerViewItems.size() / mNativeAds.size()) + 1;
        int index = 0;
        for (NativeAd ad : mNativeAds) {
            mRecyclerViewItems.add(index, ad);
            index = index + offset;
        }
    }

    private void loadDataToList() {
        if (adapter == null) {
            adapter = new YouTubeListAdapter((Activity) context, mRecyclerViewItems);
            videoRecyclerList.setAdapter(adapter);
            videoRecyclerList.scheduleLayoutAnimation();

        } else {
            adapter.notifyDataSetChanged();
        }
        videoRecyclerList.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
        // move to next load screen
    }

    private void loadNativeAd(final int adLoadCount) {

        if (adLoadCount >= NUMBER_OF_ADS) {
            insertAdsInMenuItems();
            loadDataToList();
            return;
        }

        AdLoader.Builder builder = new AdLoader.Builder(this, AddMobConfig.AD_MOB_BANNER);
        AdLoader adLoader = builder.forAppInstallAd(ad -> {
            // An app install ad loaded successfully, call this method again to
            // load the next ad.
            mNativeAds.add(ad);
            loadNativeAd(adLoadCount + 1);

        }).forContentAd(ad -> {
            // A content ad loaded successfully, call this method again to
            // load the next ad.
            mNativeAds.add(ad);
            loadNativeAd(adLoadCount + 1);
        }).withAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(int errorCode) {
                // A native ad failed to load. Call this method again to load
                // the next ad in the items list.
                Log.e("MainActivity", "The previous native ad failed to load. Attempting to"
                        + " load another.");
                loadNativeAd(adLoadCount + 1);
            }
        }).build();

        // Load the Native Express ad.
        adLoader.loadAd(new AdRequest.Builder().build());
    }
}
