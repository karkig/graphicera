package in.myvocabs.activity.search;

import android.content.Context;
import android.content.Intent;
import android.database.MatrixCursor;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.text.Html;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.jakewharton.rxbinding.view.RxView;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.myvocabs.R;
import in.myvocabs.adapters.search.DictionarySearchAdapter;
import in.myvocabs.config.OxfordConfig;
import in.myvocabs.constants.Constants;
import in.myvocabs.db.Dictionary;
import in.myvocabs.db.DictionaryDB;
import in.myvocabs.model.dictionary.Entry;
import in.myvocabs.model.dictionary.Example;
import in.myvocabs.model.dictionary.LexicalEntry;
import in.myvocabs.model.dictionary.Pronunciation;
import in.myvocabs.model.dictionary.Result;
import in.myvocabs.model.dictionary.Sense;
import in.myvocabs.model.dictionary.WordInformationResponse;
import in.myvocabs.model.searchlist.CustomSearchModel;
import in.myvocabs.network.APIService;
import in.myvocabs.network.RequestResponses;
import in.myvocabs.util.AppLog;
import in.myvocabs.util.FileManager;
import retrofit2.Response;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class SearchableActivity extends AppCompatActivity {

    @BindView(R.id.word)
    TextView word;

    @BindView(R.id.definition)
    TextView definition;

    @BindView(R.id.example)
    TextView example;

    @BindView(R.id.prnImg)
    TextView prnImg;
    @BindView(R.id.dataContainer)
    LinearLayout dataContainer;
    @BindView(R.id.prnText)
    TextView prnText;
    @BindView(R.id.searchDictionary)
    SearchView searchDictionary;
    @BindView(R.id.progress)
    ProgressBar progress;
    @BindView(R.id.webViewDictionary)
    WebView webViewDictionary;

    @BindView(R.id.wordInclude)
    LinearLayout wordInclude;
    @BindView(R.id.webViewInclude)
    LinearLayout webViewInclude;
    private Context context;
    //  String searchWord;

    private String defaultServiewText = "Dictionary";
    private List<Dictionary> wordsList = new ArrayList<>();
    private DictionarySearchAdapter searchAdapter;
    private String[] columnsSearch = new String[]{"_id", "word"};
    private MatrixCursor cursorSearch = new MatrixCursor(columnsSearch);

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_searchable);
        ButterKnife.bind(this);
        context = this;
        if (getIntent() != null) {
            Intent intent = getIntent();
            defaultServiewText = intent.getStringExtra(Constants.INTENT_SEARCH_WORD);
            searchDictionary.setQuery(defaultServiewText, false);
            if (null != defaultServiewText) {
                getWordDetail(defaultServiewText);
            }
        }
        setupSearch();
        clickListeners();
        initWebview();

    }

    private void clickListeners() {
        prnImg.setOnClickListener(v -> {
            MediaPlayer mp = new MediaPlayer();
            try {
                mp.setDataSource(prnImg.getTag().toString());
                mp.prepare();
                mp.start();
            } catch (Exception e) {
                try {
                    AppLog.e("------- playng form web");
                    mp.setDataSource(context, Uri.parse(prnText.getTag().toString()));
                    mp.prepare();
                    mp.start();
                } catch (Exception e1) {
                    e1.printStackTrace();
                }

                e.printStackTrace();
            }
        });
    }

    void setSearchView() {
        searchDictionary.setFocusable(false);
        searchDictionary.clearFocus();
        searchDictionary.setQueryHint(defaultServiewText);
        searchDictionary.setQuery(defaultServiewText, false);
    }

    private void prepareSearchItems(Dictionary newWords[]) {
        wordsList.clear();
        wordsList.addAll(Arrays.asList(newWords));
        cursorSearch = new MatrixCursor(columnsSearch);

        for (int i = 0; i < wordsList.size(); i++) {
            cursorSearch.addRow(new Object[]{0, wordsList.get(i).getWord()});
        }
        if (searchAdapter == null) {
            searchAdapter = new DictionarySearchAdapter(context, cursorSearch, wordsList);
        } else {
            searchAdapter.notifyDataSetChanged();
        }

        searchDictionary.setOnSuggestionListener(new SearchView.OnSuggestionListener() {
            @Override
            public boolean onSuggestionSelect(int position) {
                return false;
            }

            @Override
            public boolean onSuggestionClick(int position) {
                AppLog.e("------------  onSuggestionClick" + position);
                defaultServiewText = wordsList.get(position).getWord();
                setSearchView();
                searchDictionary.setQuery(defaultServiewText, true);
                return true;
            }
        });
        searchDictionary.setSuggestionsAdapter(searchAdapter);
    }

    public void setupSearch() {

        searchDictionary.clearFocus();
        searchDictionary.setQueryHint("Dictionary");
        searchDictionary.setOnCloseListener(() -> {
            defaultServiewText = "Dictionary";
            setSearchView();
            return false;
        });
        searchDictionary.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                defaultServiewText = query;
                getWordDetail(defaultServiewText);
                setSearchView();

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                Dictionary words[] = DictionaryDB.getAppDatabase(context).getDictionaryDao().findByWord("%" + newText + "%");
                prepareSearchItems(words);
                return false;
            }
        });

    }

    private void startDownload(String url) {
        new DownloadFileAsync().execute(url);
    }

    class DownloadFileAsync extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... aurl) {
            int count;
            try {
                URL url = new URL(aurl[0]);
                URLConnection conexion = url.openConnection();
                conexion.connect();
                int lenghtOfFile = conexion.getContentLength();
                AppLog.e("ANDRO_ASYNC", "Lenght of file: " + lenghtOfFile);
                InputStream input = new BufferedInputStream(url.openStream());
                OutputStream output = new FileOutputStream(FileManager.getSoundPath(defaultServiewText));
                byte data[] = new byte[1024];
                long total = 0;
                while ((count = input.read(data)) != -1) {
                    total += count;
                    output.write(data, 0, count);
                }

                output.flush();
                output.close();
                input.close();
                return FileManager.getSoundPath(defaultServiewText);
            } catch (Exception e) {
                AppLog.e("--------------" + e.getMessage());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            prnImg.setTag("" + s);
            AppLog.e("------- downloaded " + s);

        }
    }

    public void getWordDetail(String searchWord) {
        progress.setVisibility(View.VISIBLE);
        dataContainer.setVisibility(View.GONE);
        webViewInclude.setVisibility(View.GONE);
        wordInclude.setVisibility(View.VISIBLE);
        APIService requestUrl = RequestResponses.getObservableServiceRequest(context, 0);
        requestUrl.getOxfordWordDetail(OxfordConfig.BASE_URL + "/entries/en/" + searchWord + "/definitions;pronunciations;examples;")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Response<WordInformationResponse>>() {
                    @Override
                    public void onCompleted() {
                        progress.setVisibility(View.GONE);
                        dataContainer.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        AppLog.e("-------- " + throwable.getMessage());
                        progress.setVisibility(View.GONE);
                        webViewInclude.setVisibility(View.VISIBLE);
                        wordInclude.setVisibility(View.GONE);
                        loadNewWord();

                    }

                    @Override
                    public void onNext(Response<WordInformationResponse> objectResponse) {
                        WordInformationResponse data = objectResponse.body();
                        if (data != null) {
                            updateFields(data);
                        } else {
                            throw new IllegalArgumentException();
                        }
                    }
                });
    }

    private void updateFields(WordInformationResponse data) {
        List<Result> result = data.results;
        if (result != null && result.size() > 0) {
            Result item = result.get(0);

            word.setText(Html.fromHtml(" <b><font color='#FFCB07'> " + item.word + "</font> </b> "));
            List<LexicalEntry> lex = item.lexicalEntries;
            if (lex != null && lex.size() > 0) {
                LexicalEntry lexItem = lex.get(0);
                List<Pronunciation> prnArray = lexItem.pronunciations;
                List<Entry> entriesArray = lexItem.entries;

                if (prnArray != null && prnArray.size() > 0) {
                    Pronunciation prnItem = prnArray.get(0);
                    prnText.setText(Html.fromHtml(" <b><font color='#FFCB07'> " + prnItem.phoneticSpelling + " </font> </b>"));
                    if (null != prnItem.audioFile && !prnItem.audioFile.equals("")) {
                        startDownload(prnItem.audioFile);
                        prnText.setTag(prnItem.audioFile); // if loacl download failed we will play from online
                        prnImg.setVisibility(View.VISIBLE);


                    } else {
                        prnImg.setVisibility(View.GONE);
                        prnText.setTag("");
                    }

                }

                if (entriesArray != null && entriesArray.size() > 0) {
                    Entry entryItem = entriesArray.get(0);
                    List<Sense> senceArray = entryItem.senses;
                    if (senceArray != null && senceArray.size() > 0) {

                        Sense senceItem = senceArray.get(0);
                        List<String> defArray = senceItem.definitions;
                        List<Example> exmArray = senceItem.examples;

                        String defString = "";
                        String exmString = "";
                        for (String def : defArray) {
                            defString = defString + "\n\n" + def;
                        }
                        definition.setText(Html.fromHtml("<b><font color='#FFCB07'> " + defString + " </font> </b> "));

                        for (Example exm : exmArray) {
                            exmString = exmString + "\n" + exm.text;
                        }
                        example.setText(Html.fromHtml("<b><font color='#FFCB07'> " + exmString + "</font> </b>"));
                    }
                }

            }

        } else {
            return;
        }
    }

    private void initWebview() {

        WebSettings webSettings = webViewDictionary.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webViewDictionary.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                progress.setVisibility(View.VISIBLE);
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                progress.setVisibility(View.GONE);
            }
        });

    }

    private void loadNewWord() {
        Uri uri = Uri.parse("https://www.google.co.in/search?q=" + defaultServiewText + " meaning in english");
        webViewDictionary.loadUrl(uri.toString());
    }

}