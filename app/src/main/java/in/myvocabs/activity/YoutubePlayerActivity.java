package in.myvocabs.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.myvocabs.R;
import in.myvocabs.constants.Constants;

public class YoutubePlayerActivity extends YouTubeBaseActivity {

    private Context context;
    private String videoId;
    private static final int RECOVERY_DIALOG_REQUEST = 1;
    @BindView(R.id.youtubeView)
    YouTubePlayerView youtubeView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_video_player);
        ButterKnife.bind(this);
        context = this;
        if (getIntent().hasExtra(Constants.INTENT_VIDEO_ID)) {
            videoId = getIntent().getStringExtra(Constants.INTENT_VIDEO_ID);
            youtubeView.initialize(Constants.API_KEY, new YouTubePlayer.OnInitializedListener() {
                @Override
                public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
                    if (!b) {
                        youTubePlayer.loadVideo(videoId);
                        youTubePlayer.setPlayerStyle(YouTubePlayer.PlayerStyle.DEFAULT);
                    }
                }

                @Override
                public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
                    if (youTubeInitializationResult.isUserRecoverableError()) {
                        youTubeInitializationResult.getErrorDialog((Activity) context, RECOVERY_DIALOG_REQUEST).show();
                    } else {
                        Toast.makeText(context, "my error", Toast.LENGTH_LONG).show();
                    }
                }

            });
        }else {

        }

        initView();
        clickListeners();
    }

    private void clickListeners() {

    }

    private void initView() {

    }


}
