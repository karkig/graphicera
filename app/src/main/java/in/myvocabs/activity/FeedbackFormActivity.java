package in.myvocabs.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.jakewharton.rxbinding.view.RxView;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.myvocabs.R;
import in.myvocabs.config.AddMobConfig;
import in.myvocabs.model.BaseResponse;
import in.myvocabs.network.APIService;
import in.myvocabs.network.RequestResponses;
import in.myvocabs.util.AppLog;
import in.myvocabs.util.Validator;
import retrofit2.Response;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by kailash on 3/22/2016.
 */
public class FeedbackFormActivity extends Activity {

    @BindView(R.id.send)
    FloatingActionButton send;

    @BindView(R.id.edtFeedBack)
    EditText edtFeedBack;

    @BindView(R.id.ADD_SCREEN)
    LinearLayout ADD_SCREEN;

    @BindView(R.id.adView)
    AdView adView;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.feedback_activity);
        getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        ButterKnife.bind(this);
        context = this;
        loadAd();
        init();
        clickListener();
    }

    private void clickListener() {

        RxView.clicks(send).subscribe(aVoid -> {

            if (edtFeedBack.getText() != null && Validator.isValidString(edtFeedBack.getText().toString())) {

            } else {
                edtFeedBack.setHint("Please enter some suggestion or feedback to us.");
                return;
            }
            sendFeedback(edtFeedBack.getText().toString());
        });
    }

    private void init() {
    }


    private void loadAd() {
        ADD_SCREEN.removeAllViews();
        AppLog.e("------------- " + AddMobConfig.AD_MOB_BANNER);
        ADD_SCREEN.setVisibility(View.VISIBLE);
        //adView.setAdSize(AdSize.FULL_BANNER);
        //adView.setAdUnitId(AddMobConfig.AD_MOB_BANNER);
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);

    }

    private void setUpAnimation(boolean isRotate) {
        Animation rotation = AnimationUtils.loadAnimation(context, R.anim.rotate_progress);
        rotation.setFillAfter(true);
        send.startAnimation(isRotate ? rotation : null);
    }

    public void sendFeedback(String feedback) {
        setUpAnimation(true);
        HashMap<String, String> map = new HashMap<>();
        map.put("feedback", feedback);
        APIService requestUrl = RequestResponses.getObservableServiceRequest(context, 0);
        requestUrl.feedback(map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Response<BaseResponse>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        AppLog.e("-------- " + throwable.getMessage());
                        finish();
                    }

                    @Override
                    public void onNext(Response<BaseResponse> objectResponse) {

                        BaseResponse data = objectResponse.body();
                        if (data != null && data.status) {
                            Toast.makeText(context, data.message, Toast.LENGTH_LONG).show();
                            finish();

                        } else {

                        }
                    }
                });
    }
}
