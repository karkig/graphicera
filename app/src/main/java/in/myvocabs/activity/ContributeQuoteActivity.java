package in.myvocabs.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.jakewharton.rxbinding.view.RxView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.myvocabs.R;
import in.myvocabs.config.AddMobConfig;
import in.myvocabs.model.BaseResponse;
import in.myvocabs.model.debate.Tag;
import in.myvocabs.network.APIService;
import in.myvocabs.network.RequestResponses;
import in.myvocabs.util.AppLog;
import in.myvocabs.util.Validator;
import retrofit2.Response;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by kailash on 3/22/2016.
 */
public class ContributeQuoteActivity extends Activity {

    @BindView(R.id.sendQuote)
    FloatingActionButton sendQuote;

    @BindView(R.id.edtQuote)
    EditText edtQuote;
    @BindView(R.id.edtAuthor)
    EditText edtAuthor;

    @BindView(R.id.ADD_SCREEN)
    LinearLayout ADD_SCREEN;

    @BindView(R.id.tagContainer)
    LinearLayout tagContainer;

    @BindView(R.id.adView)
    AdView adView;

    Context context;
    List<Tag> tagsList = new ArrayList<>();
    String selctedCat = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contribute_quote_dialog_fragment);
        getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        ButterKnife.bind(this);
        context = this;
        init();
        clickListener();
        loadAd();

    }

    private void clickListener() {

        RxView.clicks(sendQuote).subscribe(aVoid -> {

            if (edtQuote.getText() != null && Validator.isValidString(edtQuote.getText().toString())) {

            } else {
                edtQuote.setHint("Please enter a quote");
                return;
            }

            if (edtAuthor.getText() != null && Validator.isValidString(edtAuthor.getText().toString())) {
            } else {
                edtAuthor.setHint("Please enter a author name");
                return;
            }

            addQuoteContribute(edtQuote.getText().toString(), edtAuthor.getText().toString(), selctedCat);
        });
    }

    private void init() {
        String[] tagArray = context.getResources().getStringArray(R.array.debate_tag_list);
        tagsList.clear();
        for (String s : tagArray) {
            Tag tag = new Tag();
            tag.setEnable(true);
            tag.setPriority(1);
            tag.setTg(s);
            tagsList.add(tag);
        }
        createTagsLayout();
    }

    public void createTagsLayout() {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        tagContainer.removeAllViews();
        for (Tag t : tagsList) {
            LinearLayout tagView = (LinearLayout) inflater.inflate(R.layout.quote_contribubte_tag_layout_item, null);

            TextView text = (TextView) tagView.findViewById(R.id.tag);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(10, 10, 10, 10);//pass int values for left,top,right,bottom
            text.setLayoutParams(params);

            text.setText(t.getTg() + "");
            tagView.setOnClickListener(v -> {
                selctedCat = text.getText().toString();
                text.setTextColor(context.getResources().getColor(R.color.colorPrimary));

                AppLog.e("----");
            });
            tagContainer.addView(tagView);
        }
    }

    private void loadAd() {
        ADD_SCREEN.removeAllViews();
        ADD_SCREEN.setVisibility(View.VISIBLE);
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);

    }

    private void setUpAnimation(boolean isRotate) {
        Animation rotation = AnimationUtils.loadAnimation(context, R.anim.rotate_progress);
        rotation.setFillAfter(true);
        sendQuote.startAnimation(isRotate ? rotation : null);
    }

    public void addQuoteContribute(String quote, String au, String cat) {
        setUpAnimation(true);
        HashMap<String, String> map = new HashMap<>();
        map.put("quote", quote);
        map.put("au_name", au);
        map.put("cat", cat);

        APIService requestUrl = RequestResponses.getObservableServiceRequest(context, 0);
        requestUrl.addQuoteContribute(map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Response<BaseResponse>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        AppLog.e("-------- " + throwable.getMessage());
                        finish();
                    }

                    @Override
                    public void onNext(Response<BaseResponse> objectResponse) {

                        BaseResponse data = objectResponse.body();
                        if (data != null && data.status) {
                            Toast.makeText(context, data.message, Toast.LENGTH_LONG).show();
                            finish();

                        } else {

                        }
                    }
                });
    }
}
