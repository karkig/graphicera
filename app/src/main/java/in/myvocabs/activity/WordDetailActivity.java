package in.myvocabs.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.jakewharton.rxbinding.view.RxView;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.myvocabs.R;
import in.myvocabs.config.AddMobConfig;
import in.myvocabs.constants.Constants;
import in.myvocabs.model.BaseResponse;
import in.myvocabs.model.word.Data;
import in.myvocabs.network.APIService;
import in.myvocabs.network.RequestResponses;
import in.myvocabs.util.AppLog;
import in.myvocabs.util.Validator;
import retrofit2.Response;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by kailash on 3/22/2016.
 */
public class WordDetailActivity extends Activity {

    @BindView(R.id.cancel)
    FloatingActionButton cancel;

    @BindView(R.id.txtExample)
    TextView txtExample;
    @BindView(R.id.txtMeaning)
    TextView txtMeaning;
    @BindView(R.id.txtWord)
    TextView txtWord;
    @BindView(R.id.adView)
    AdView adView;
    Context context;
    Data wordData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_word_detail);
        getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        ButterKnife.bind(this);
        context = this;
        if (getIntent() != null) {
            wordData = getIntent().getParcelableExtra(Constants.INTENT_WORD_DATA);
            init();
            clickListener();
        } else {

        }
        loadAd();

    }

    private void clickListener() {

        RxView.clicks(cancel).subscribe(aVoid -> {
            finish();
        });
    }

    private void init() {
        if (null != wordData) {
            txtExample.setText(wordData.getExmpl() + "");
            txtMeaning.setText(wordData.getMeaning() + "");
            txtWord.setText(wordData.getWord() + "");
        }
    }

    private void loadAd() {
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);

    }

}
