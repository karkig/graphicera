package in.myvocabs.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import in.myvocabs.R;
import in.myvocabs.db.DictionaryDB;
import in.myvocabs.db.dao.DictionaryDao;
import in.myvocabs.services.DbCreateIntentService;
import in.myvocabs.util.AppLog;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        startActivity(new Intent(this, HomeActivity.class));
        finish();
    }
}
