package in.myvocabs.adapters.search;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import in.myvocabs.R;
import in.myvocabs.db.Dictionary;
import in.myvocabs.util.AppLog;

/**
 * Created by kailash on 24/2/18.
 */

public class DictionarySearchAdapter extends CursorAdapter {

    private List<Dictionary> items;

    private TextView text;
    Context mContext;
    Cursor mCursor;

    public DictionarySearchAdapter(Context context, Cursor cursor, List<Dictionary> items) {
        super(context, cursor, false);
        this.items = items;
        mContext = context;
        mCursor = cursor;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        try {
            text.setText(items.get(cursor.getPosition()).getWord());
        } catch (Exception e) {
            AppLog.e("---------------- " + e.getMessage());
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v;
        if (convertView == null) {
            v = newView(mContext, mCursor, parent);
        } else {
            v = convertView;
        }
        bindView(v, mContext, mCursor);

        return v;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        try {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.search_item, parent, false);
            text = (TextView) view.findViewById(R.id.searchItem);
            return view;
        } catch (Exception e) {
            AppLog.e("---------------- " + e.getMessage());
        }

        return null;
    }

}