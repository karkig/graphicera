package in.myvocabs.constants;

import in.myvocabs.config.ProjectConfig;
import in.myvocabs.config.ProjectEnvironment;

/**
 * Created by godoctor on 21/3/17.
 */

public class Constants {
    public static String BASE_URL;
    public static final int NORMAL_REQUEST = 0;
    public static final int IMAGE_REQUEST = 1;
    public static final String SIMPLE_DATE_REGEX = "dd/MM/yyyy', 'hh:mm a";
    public static final String DATABASE_NAME = "english_updates";
    public static final String ENG_UPDATES_DIR_NAME = "EnglishUpdates";

    public static final int BBC_NEWS = 1;
    public static final int TIMES_OF_INDIA_NEWS = 2;
    public static final int TECHNICAL_WORLD_NEWS = 3;
    public static final int OPEN_SITE = 4;

    public static final String SEARCh_YOUTUBE_VIDEO_URL = "https://www.googleapis.com/youtube/v3/search";
    public static final String GET_WEATHER_URL = "http://api.openweathermap.org/data/2.5/weather";
    // public static final String GET_WORD_OF_THE_DAY = "http://urban-word-of-the-day.herokuapp.com";
    //public static final String GET_QUOTE_OF_THE_DAY = "https://talaikis.com/api/quotes/random/";

    public static final String GET_WORD_OF_THE_DAY = "get-word";
    public static final String GET_QUOTE_OF_THE_DAY = "get-quote";
    public static final String POST_QUOTE_LIKE = "plus-like-quote";
    public static final String POST_WORD_LIKE = "plus-like-word";
    public static final String POST_UPDATE_APP = "get-app-version";
    public static final String POST_ADD_QUOTE_CONTRIBUTE = "add-quote";
    public static final String POST_FEEDBACK = "feedback";


    public static final String PREF_WORD_OF_THE_DAY = "PREF_WORD_OF_THE_DAY";
    public static final String PREF_WORD_OF_THE_DAY_MEANING = "PREF_WORD_OF_THE_DAY_MEANING";

    public static final String PREF_QUOTE_OF_THE_DAY = "PREF_QUOTE_OF_THE_DAY";
    public static final String PREF_QUOTE_OF_THE_DAY_SAID_BY = "PREF_QUOTE_OF_THE_DAY_SAID_BY";


    public static final String GET_DEBATE_TAG = "get-debate-tags";

    public static String API_KEY = "AIzaSyD3_V_rwlvDwIaW1ZJmj0fIhgVzphbaGI8"; //AIzaSyChOBWDZ3_uHgjWZEoGDLgpEtrletUMN9k
    //public  static  String MOB_PUB_ADS_KEY="1eaddc895bf24210b2b9fe6c4747734d";
    public static String OPEN_WEATHER_MAP_API_KEY = "779f7a9e5d38530578ecdc96f23fbfbf";


    // Intents
    public static final String INTENT_YOUTUBE_LIST_DATA = "youtube_intent_data";
    public static final String INTENT_WORD_DATA = "word_of_the_day_intent";
    public static final String INTENT_SEARCH_WORD = "searched_word";


    // other constants
    public static String NOTIFICATIONS_CHANNEL_ID_WORD = "NOTIFICATIONS_CHANNEL_ID_WORD";

    public static String NOTIFICATIONS_CHANNEL_ID_QUOTE = "NOTIFICATIONS_CHANNEL_ID_QUOTE";

    public static String SNIPPET_PARAMS = "snippet";
    public static String MAX_RESULTS_PARAMS = "maxResults";
    public static String INTENT_DOWNLOAD_URL = "downloading_url";
    public static String INTENT_TITLE = "song_title";
    public static String INTENT_BUNDLE = "bundle";
    public static String INTENT_SONG_DETAIL = "song_details";
    public static String INTENT_VIDEO_ID = "videoID";
    public static String INTENT_IS_VIDEO_CONVERTED = "isvideoConverted";
    public static String INTENT_RESPONSE = "video_list";
    public static final String FILE_NAME = "SharedPreferencesData";
    public static final String IS_UPDATE_SHOWN = "is_update_show";
    public static final String PLATFORM = "android";

    //Pref

    public static final String PREF_IS_DICTIONARY_CREATED = "is_dictionary_created";

    /**
     *   Do not change the server url from here. To change goto ProjectConfig.java and change the project environment to
     *   ProjectEnvironment.STAG, ProjectEnvironment.PROD, ProjectEnvironment.DEV
     */
    static {
        ProjectEnvironment env = ProjectConfig.getEnv();
        if (env.environment.equals(ProjectEnvironment.DEV.environment)) {
            BASE_URL = "http://192.168.0.38:8080/english-updates/v1/";
        } else if (env.environment.equals(ProjectEnvironment.STAG.environment)) {
        } else if (env.environment.equals(ProjectEnvironment.PROD.environment)) {
            BASE_URL = "http://35.154.57.92:8080/english-updates/v1/";
        }
    }
}
