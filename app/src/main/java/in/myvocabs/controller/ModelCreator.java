package in.myvocabs.controller;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import in.myvocabs.model.searchlist.CustomSearchModel;
import in.myvocabs.model.youtube.Item;
import in.myvocabs.model.youtube.Snippet;
import in.myvocabs.model.youtube.VideoSearchResponse;
import in.myvocabs.util.AppUtils;

/**
 * Created by karki on 12/6/16.
 */
public class ModelCreator {
    private static ArrayList<CustomSearchModel> list = new ArrayList<>();

    public static ArrayList<CustomSearchModel> createListModel(VideoSearchResponse response) {
        list.clear();
        for (Item item : response.getItems()) {
            if (item.getId().getKind().equalsIgnoreCase("youtube#video")) {
                CustomSearchModel searchModel = new CustomSearchModel();
                Snippet snippet = item.getSnippet();
                searchModel.setVideoId(item.getId().getVideoId());
                searchModel.setDescription(snippet.getDescription());
                searchModel.setImgUrl(snippet.getThumbnails().getMedium().getUrl());
                searchModel.setTitle(snippet.getTitle());
                searchModel.setPublishedOn(snippet.getPublishedAt());
                searchModel.setDate(AppUtils.formatDate(searchModel.getPublishedOn()));
                list.add(searchModel);
            }
        }
        Collections.sort(list, (o1, o2) -> o2.getDate().compareTo(o1.getDate()));
        return list;
    }
}

