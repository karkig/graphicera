package in.myvocabs.db;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.content.Context;
import android.support.annotation.NonNull;

import in.myvocabs.constants.Constants;
import in.myvocabs.db.creator.ListCreator;
import in.myvocabs.db.dao.DictionaryDao;
import in.myvocabs.util.AppLog;

/**
 * Created by kailash on 22/2/18.
 */

@Database(entities = {Dictionary.class}, version = 1, exportSchema = false)
public abstract class DictionaryDB extends RoomDatabase {

    private static DictionaryDB INSTANCE;

    public abstract DictionaryDao getDictionaryDao();

    public static DictionaryDB getAppDatabase(Context context) {
        if (INSTANCE == null) {
            synchronized (DictionaryDB.class) {
                INSTANCE = Room.databaseBuilder(context.getApplicationContext(), DictionaryDB.class, Constants.DATABASE_NAME)
                        .addCallback(new Callback() {
                            @Override
                            public void onCreate(@NonNull SupportSQLiteDatabase db) {
                                super.onCreate(db);

                                AppLog.e("---- db crated");
                            }

                            @Override
                            public void onOpen(@NonNull SupportSQLiteDatabase db) {
                                super.onOpen(db);
                                AppLog.e("---- db opend");

                            }
                        })
                        .allowMainThreadQueries()
                        .build();
            }
        }
        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }
}
