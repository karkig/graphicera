package in.myvocabs.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import in.myvocabs.db.Dictionary;

/**
 * Created by kailash on 22/2/18.
 */

@Dao
public interface DictionaryDao {

    @Query("SELECT * FROM Dictionary WHERE word LIKE :wordData LIMIT 15")
    Dictionary[] findByWord(String wordData);


    @Insert
    void insert(Dictionary dictionary);

    @Delete
    void delete(Dictionary dictionary);

    @Query("SELECT * FROM Dictionary")
    Dictionary[] getAll();

}
