package in.myvocabs.model.word;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import in.myvocabs.model.BaseResponse;

/**
 * Created by kailash on 12/2/18.
 */

public class Data implements Parcelable {

    @SerializedName("word")
    @Expose
    public String word;
    @SerializedName("meaning")
    @Expose
    public String meaning;
    @SerializedName("likes")
    @Expose
    public Integer likes;
    @SerializedName("level")
    @Expose
    public Integer level;
    @SerializedName("synonyms")
    @Expose
    public List<Object> synonyms = new ArrayList<Object>();
    @SerializedName("antonyms")
    @Expose
    public List<Object> antonyms = new ArrayList<Object>();
    @SerializedName("exams")
    @Expose
    public List<Object> exams = new ArrayList<Object>();
    @SerializedName("date")
    @Expose
    public String date;
    @SerializedName("exmpl")
    @Expose
    public String exmpl;

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public String getMeaning() {
        return meaning;
    }

    public void setMeaning(String meaning) {
        this.meaning = meaning;
    }

    public Integer getLikes() {
        return likes;
    }

    public void setLikes(Integer likes) {
        this.likes = likes;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public List<Object> getSynonyms() {
        return synonyms;
    }

    public void setSynonyms(List<Object> synonyms) {
        this.synonyms = synonyms;
    }

    public List<Object> getAntonyms() {
        return antonyms;
    }

    public void setAntonyms(List<Object> antonyms) {
        this.antonyms = antonyms;
    }

    public List<Object> getExams() {
        return exams;
    }

    public void setExams(List<Object> exams) {
        this.exams = exams;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getExmpl() {
        return exmpl;
    }

    public void setExmpl(String exmpl) {
        this.exmpl = exmpl;

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.word);
        dest.writeString(this.meaning);
        dest.writeValue(this.likes);
        dest.writeValue(this.level);
        dest.writeList(this.synonyms);
        dest.writeList(this.antonyms);
        dest.writeList(this.exams);
        dest.writeString(this.date);
        dest.writeString(this.exmpl);
    }

    public Data() {
    }

    protected Data(Parcel in) {
        this.word = in.readString();
        this.meaning = in.readString();
        this.likes = (Integer) in.readValue(Integer.class.getClassLoader());
        this.level = (Integer) in.readValue(Integer.class.getClassLoader());
        this.synonyms = new ArrayList<Object>();
        in.readList(this.synonyms, Object.class.getClassLoader());
        this.antonyms = new ArrayList<Object>();
        in.readList(this.antonyms, Object.class.getClassLoader());
        this.exams = new ArrayList<Object>();
        in.readList(this.exams, Object.class.getClassLoader());
        this.date = in.readString();
        this.exmpl = in.readString();
    }

    public static final Parcelable.Creator<Data> CREATOR = new Parcelable.Creator<Data>() {
        @Override
        public Data createFromParcel(Parcel source) {
            return new Data(source);
        }

        @Override
        public Data[] newArray(int size) {
            return new Data[size];
        }
    };
}
