package in.myvocabs.model.word;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import in.myvocabs.model.BaseResponse;

/**
 * Created by kailash on 18/2/18.
 */

public class WordResponse extends BaseResponse implements Parcelable {
    @SerializedName("data")
    @Expose
    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public static Creator<WordResponse> getCREATOR() {
        return CREATOR;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.data, flags);
    }

    public WordResponse() {
    }

    protected WordResponse(Parcel in) {
        this.data = in.readParcelable(Data.class.getClassLoader());
    }

    public static final Parcelable.Creator<WordResponse> CREATOR = new Parcelable.Creator<WordResponse>() {
        @Override
        public WordResponse createFromParcel(Parcel source) {
            return new WordResponse(source);
        }

        @Override
        public WordResponse[] newArray(int size) {
            return new WordResponse[size];
        }
    };
}