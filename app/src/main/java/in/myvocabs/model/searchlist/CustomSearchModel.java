package in.myvocabs.model.searchlist;

import android.os.Parcel;
import android.os.Parcelable;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import in.myvocabs.util.AppLog;

/**
 * Created by karki on 12/6/16.
 */
public class CustomSearchModel implements Parcelable {

    private String videoId;
    private String title;
    private String description;
    private String imgUrl;
    private String publishedOn;
    private Date date;

    public String getVideoId() {

        return videoId;
    }

    public CustomSearchModel() {

    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public void setVideoId(String videoId) {
        this.videoId = videoId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getPublishedOn() {
        return publishedOn;
    }

    public void setPublishedOn(String publishedOn) {
        this.publishedOn = publishedOn;
    }

    public static Creator<CustomSearchModel> getCREATOR() {
        return CREATOR;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.videoId);
        dest.writeString(this.title);
        dest.writeString(this.description);
        dest.writeString(this.imgUrl);
        dest.writeString(this.publishedOn);
    }

    protected CustomSearchModel(Parcel in) {
        this.videoId = in.readString();
        this.title = in.readString();
        this.description = in.readString();
        this.imgUrl = in.readString();
        this.publishedOn = in.readString();
    }

    public static final Parcelable.Creator<CustomSearchModel> CREATOR = new Parcelable.Creator<CustomSearchModel>() {
        @Override
        public CustomSearchModel createFromParcel(Parcel source) {
            return new CustomSearchModel(source);
        }

        @Override
        public CustomSearchModel[] newArray(int size) {
            return new CustomSearchModel[size];
        }
    };


}