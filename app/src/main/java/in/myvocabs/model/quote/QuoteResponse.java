package in.myvocabs.model.quote;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import in.myvocabs.model.BaseResponse;

/**
 * Created by kailash on 7/2/18.
 */

public class QuoteResponse extends BaseResponse{
    @SerializedName("data")
    @Expose
    public Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}