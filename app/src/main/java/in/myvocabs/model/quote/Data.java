package in.myvocabs.model.quote;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by kailash on 18/2/18.
 */

public class Data {

    @SerializedName("cat")
    @Expose
    public String cat;
    @SerializedName("quote")
    @Expose
    public String quote;
    @SerializedName("likes")
    @Expose
    public Integer likes;
    @SerializedName("author")
    @Expose
    public String author;
    @SerializedName("date")
    @Expose
    public String date;

    public String getCat() {
        return cat;
    }

    public void setCat(String cat) {
        this.cat = cat;
    }

    public String getQuote() {
        return quote;
    }

    public void setQuote(String quote) {
        this.quote = quote;
    }

    public Integer getLikes() {
        return likes;
    }

    public void setLikes(Integer likes) {
        this.likes = likes;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}