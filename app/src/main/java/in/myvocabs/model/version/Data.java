package in.myvocabs.model.version;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by kailash on 19/2/18.
 */

public class Data implements Parcelable {

    private String plfrm;
    private String vname;
    private Integer vcode;
    private Integer critclVcode;

    public String getPlfrm() {
        return plfrm;
    }

    public void setPlfrm(String plfrm) {
        this.plfrm = plfrm;
    }

    public String getVname() {
        return vname;
    }

    public void setVname(String vname) {
        this.vname = vname;
    }

    public Integer getVcode() {
        return vcode;
    }

    public void setVcode(Integer vcode) {
        this.vcode = vcode;
    }

    public Integer getCritclVcode() {
        return critclVcode;
    }

    public void setCritclVcode(Integer critclVcode) {
        this.critclVcode = critclVcode;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.plfrm);
        dest.writeString(this.vname);
        dest.writeValue(this.vcode);
        dest.writeValue(this.critclVcode);
    }

    public Data() {
    }

    protected Data(Parcel in) {
        this.plfrm = in.readString();
        this.vname = in.readString();
        this.vcode = (Integer) in.readValue(Integer.class.getClassLoader());
        this.critclVcode = (Integer) in.readValue(Integer.class.getClassLoader());
    }

    public static final Parcelable.Creator<Data> CREATOR = new Parcelable.Creator<Data>() {
        @Override
        public Data createFromParcel(Parcel source) {
            return new Data(source);
        }

        @Override
        public Data[] newArray(int size) {
            return new Data[size];
        }
    };
}