package in.myvocabs.model.version;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by kailash on 19/2/18.
 */

public class VersionResponse implements Parcelable {
    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.data, flags);
    }

    public VersionResponse() {
    }

    protected VersionResponse(Parcel in) {
        this.data = in.readParcelable(Data.class.getClassLoader());
    }

    public static final Parcelable.Creator<VersionResponse> CREATOR = new Parcelable.Creator<VersionResponse>() {
        @Override
        public VersionResponse createFromParcel(Parcel source) {
            return new VersionResponse(source);
        }

        @Override
        public VersionResponse[] newArray(int size) {
            return new VersionResponse[size];
        }
    };
}
