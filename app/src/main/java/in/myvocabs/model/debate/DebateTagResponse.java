package in.myvocabs.model.debate;

import in.myvocabs.model.BaseResponse;

/**
 * Created by kailash on 19/2/18.
 */

public class DebateTagResponse  extends BaseResponse{


    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}