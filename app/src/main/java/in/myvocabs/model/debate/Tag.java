package in.myvocabs.model.debate;

/**
 * Created by kailash on 19/2/18.
 */

public class Tag {

    private String tg;
    private Boolean enable;
    private int priority;
    public String getTg() {
        return tg;
    }

    public void setTg(String tg) {
        this.tg = tg;
    }

    public Boolean getEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }
}