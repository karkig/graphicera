package in.myvocabs.model.debate;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kailash on 19/2/18.
 */

public class Data  {

    private List<Tag> tags = new ArrayList<Tag>();

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }
}