package in.myvocabs.model.dictionary;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kailash on 26/2/18.
 */

public class WordInformationResponse {

    @SerializedName("metadata")
    @Expose
    public Metadata metadata;
    @SerializedName("results")
    @Expose
    public List<Result> results = new ArrayList<Result>();

}