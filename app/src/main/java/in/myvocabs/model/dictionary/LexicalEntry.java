package in.myvocabs.model.dictionary;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kailash on 26/2/18.
 */

public class LexicalEntry {

    @SerializedName("entries")
    @Expose
    public List<Entry> entries = new ArrayList<Entry>();
    @SerializedName("language")
    @Expose
    public String language;
    @SerializedName("lexicalCategory")
    @Expose
    public String lexicalCategory;
    @SerializedName("pronunciations")
    @Expose
    public List<Pronunciation> pronunciations = new ArrayList<Pronunciation>();
    @SerializedName("text")
    @Expose
    public String text;

}