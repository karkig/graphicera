package in.myvocabs.model.dictionary;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kailash on 26/2/18.
 */

public class Pronunciation {

    @SerializedName("audioFile")
    @Expose
    public String audioFile;
    @SerializedName("dialects")
    @Expose
    public List<String> dialects = new ArrayList<String>();
    @SerializedName("phoneticNotation")
    @Expose
    public String phoneticNotation;
    @SerializedName("phoneticSpelling")
    @Expose
    public String phoneticSpelling;

}