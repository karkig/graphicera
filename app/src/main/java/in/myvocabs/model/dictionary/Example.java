package in.myvocabs.model.dictionary;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by kailash on 26/2/18.
 */

public class Example {

    @SerializedName("text")
    @Expose
    public String text;

}