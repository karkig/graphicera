package in.myvocabs.model.dictionary;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kailash on 26/2/18.
 */

public class Result {

    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("language")
    @Expose
    public String language;
    @SerializedName("lexicalEntries")
    @Expose
    public List<LexicalEntry> lexicalEntries = new ArrayList<LexicalEntry>();
    @SerializedName("type")
    @Expose
    public String type;
    @SerializedName("word")
    @Expose
    public String word;

}