package in.myvocabs.model.dictionary;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kailash on 26/2/18.
 */

public class Sense {

    @SerializedName("definitions")
    @Expose
    public List<String> definitions = new ArrayList<String>();
    @SerializedName("examples")
    @Expose
    public List<Example> examples = new ArrayList<Example>();
    @SerializedName("id")
    @Expose
    public String id;

}