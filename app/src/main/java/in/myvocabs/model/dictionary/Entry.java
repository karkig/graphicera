package in.myvocabs.model.dictionary;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kailash on 26/2/18.
 */

public class Entry {

    @SerializedName("homographNumber")
    @Expose
    public String homographNumber;
    @SerializedName("senses")
    @Expose
    public List<Sense> senses = new ArrayList<Sense>();

}