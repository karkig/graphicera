package in.myvocabs.application;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.multidex.MultiDexApplication;

import com.google.android.gms.ads.MobileAds;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;

import in.myvocabs.BuildConfig;
import in.myvocabs.R;
import in.myvocabs.config.AddMobConfig;
import in.myvocabs.util.AppLog;

public class WordOfTheDayApplication extends MultiDexApplication {

    private static Context sContext;

    public static Context getAppContext() {
        return sContext;
    }

    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    public void onCreate() {
        super.onCreate();
        sContext = this;
        initImageLoader(sContext);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        if (BuildConfig.DEBUG) {
            // Test keys
            AddMobConfig.AD_MOB_APP_ID = "ca-app-pub-3940256099942544~3347511713";
            AddMobConfig.AD_MOB_BANNER = "ca-app-pub-3940256099942544/8135179316";

        } else {
            // Production keys
            AddMobConfig.AD_MOB_APP_ID = "ca-app-pub-4342075950492250~9590874597";
            AddMobConfig.AD_MOB_BANNER = "ca-app-pub-4342075950492250/4412190797";
        }
        MobileAds.initialize(this, AddMobConfig.AD_MOB_APP_ID);

    }

    private void initImageLoader(Context context) {
        try {
            DisplayImageOptions displayImageOptions = new DisplayImageOptions.Builder()
                    .cacheInMemory(true)
                    .cacheOnDisk(true)
                    .considerExifParams(true)
                    .showImageOnLoading(R.drawable.logo)
                    .showImageForEmptyUri(R.drawable.logo)
                    .showImageOnFail(R.drawable.logo)
                    .bitmapConfig(Bitmap.Config.RGB_565)
                    .displayer(new SimpleBitmapDisplayer())
                    .build();

            ImageLoaderConfiguration.Builder config = new ImageLoaderConfiguration.Builder(context);
            config.threadPriority(Thread.NORM_PRIORITY - 2);
            config.denyCacheImageMultipleSizesInMemory();
            config.diskCacheFileNameGenerator(new Md5FileNameGenerator());
            config.diskCacheSize(200 * 1024 * 1024); // 50 MiB
            config.defaultDisplayImageOptions(displayImageOptions);
            config.tasksProcessingOrder(QueueProcessingType.LIFO);

            ImageLoader.getInstance().init(config.build());
        } catch (Exception e) {
            AppLog.e("Image Loading fail:" + e.getMessage());
        }
    }
}
