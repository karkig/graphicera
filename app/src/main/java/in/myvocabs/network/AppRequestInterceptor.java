package in.myvocabs.network;


import java.util.concurrent.TimeUnit;

import in.myvocabs.BuildConfig;
import in.myvocabs.constants.Constants;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;

public class AppRequestInterceptor {

    public static OkHttpClient getRequestInterceptor(int state) {
        OkHttpClient.Builder okHttpClient = new OkHttpClient.Builder();
        if (state == Constants.NORMAL_REQUEST && BuildConfig.DEBUG) {
            okHttpClient.addInterceptor(getLoggingInterceptor());
        }
        okHttpClient.readTimeout(2, TimeUnit.MINUTES);
        okHttpClient.connectTimeout(2, TimeUnit.MINUTES);
        return okHttpClient.build();
    }

    private static HttpLoggingInterceptor getLoggingInterceptor() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        return logging;
    }
}

