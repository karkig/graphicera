package in.myvocabs.network;

import java.util.HashMap;
import java.util.Map;

import in.myvocabs.config.OxfordConfig;
import in.myvocabs.constants.Constants;
import in.myvocabs.model.BaseResponse;
import in.myvocabs.model.debate.DebateTagResponse;
import in.myvocabs.model.dictionary.WordInformationResponse;
import in.myvocabs.model.quote.QuoteResponse;
import in.myvocabs.model.version.VersionResponse;
import in.myvocabs.model.weather.WeatherResponse;
import in.myvocabs.model.word.Data;
import in.myvocabs.model.word.WordResponse;
import in.myvocabs.model.youtube.VideoSearchResponse;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Headers;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;
import rx.Observable;

/**
 * Created by Piyush on 14-01-2016.
 */
public interface APIService {


    @retrofit2.http.GET(Constants.SEARCh_YOUTUBE_VIDEO_URL)
    Observable<Response<VideoSearchResponse>> getVideoListFromYoutube(@QueryMap Map<String, String> params);

    @retrofit2.http.GET(Constants.GET_WEATHER_URL)
    Observable<Response<WeatherResponse>> getWeatherData(@QueryMap Map<String, String> params);

    @retrofit2.http.GET(Constants.GET_WORD_OF_THE_DAY)
    Observable<Response<WordResponse>> getWordOfTheDay();

    @FormUrlEncoded
    @retrofit2.http.POST(Constants.POST_QUOTE_LIKE)
    Observable<Response<BaseResponse>> updateQuoteLike(@FieldMap HashMap<String, Boolean> map);


    @FormUrlEncoded
    @retrofit2.http.POST(Constants.POST_WORD_LIKE)
    Observable<Response<BaseResponse>> updateWordLike(@FieldMap HashMap<String, Boolean> map);

    @retrofit2.http.GET(Constants.GET_QUOTE_OF_THE_DAY)
    Observable<Response<QuoteResponse>> getQuoteOfTheDay();


    @FormUrlEncoded
    @retrofit2.http.POST(Constants.POST_UPDATE_APP)
    Observable<Response<VersionResponse>> updateAppVersion(@FieldMap HashMap<String, String> map);

    @retrofit2.http.GET(Constants.GET_DEBATE_TAG)
    Observable<Response<DebateTagResponse>> getDebateTags();

    @FormUrlEncoded
    @retrofit2.http.POST(Constants.POST_ADD_QUOTE_CONTRIBUTE)
    Observable<Response<BaseResponse>> addQuoteContribute(@FieldMap HashMap<String, String> map);

    @FormUrlEncoded
    @retrofit2.http.POST(Constants.POST_FEEDBACK)
    Observable<Response<BaseResponse>> feedback(@FieldMap HashMap<String, String> map);

    @Headers({
            "app_id:" + OxfordConfig.Application_ID + "",
            "app_key:" + OxfordConfig.Application_Key + "",
            "Accept: application/json"
    })
    @retrofit2.http.GET
    Observable<Response<WordInformationResponse>> getOxfordWordDetail(@Url String url);


}