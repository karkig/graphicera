package in.myvocabs.util;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.content.ContextCompat;

/**
 * Created by kailash on 4/5/17.
 */

public class Helper {


    public static boolean requestStoragePermission(Activity activity, int READ_STORAGE_PERMISSION) {

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        } else {

            if (ContextCompat.checkSelfPermission(activity, Manifest.permission_group.STORAGE) == PackageManager.PERMISSION_GRANTED
                    && ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                    && ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
                    ) {
                return true;

            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    activity.requestPermissions(new String[]{Manifest.permission_group.STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE,
                                    Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                            READ_STORAGE_PERMISSION);
                    return false;

                }
            }
        }
        return false;
    }


}

