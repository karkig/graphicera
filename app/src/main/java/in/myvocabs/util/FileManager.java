package in.myvocabs.util;

import android.os.Environment;

import java.io.File;

import in.myvocabs.constants.Constants;


/**
 * Created by kailash on 24/2/17.
 */

public class FileManager {

    public static final String TEMP_FILE = "temp.txt";
    public static final String WORDS_FILE = "words_list.txt";

    public static String getMeDataStoragePath() {
        File dir = new File(Environment.getExternalStorageDirectory(), Constants.ENG_UPDATES_DIR_NAME);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        return dir.getAbsolutePath() + File.separator;
    }

    public static String getSoundPath(String name) {
        return getMeDataStoragePath() + name + ".mp3";
    }

    public static String getVideoPath(String name) {
        return getMeDataStoragePath() + name + ".mp4";
    }

    public static String getLogFilePath(String name) {
        return getMeDataStoragePath() + name + ".txt";
    }
}
