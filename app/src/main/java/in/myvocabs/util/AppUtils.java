package in.myvocabs.util;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import in.myvocabs.BuildConfig;
import in.myvocabs.R;
import in.myvocabs.activity.HomeActivity;
import in.myvocabs.application.WordOfTheDayApplication;
import in.myvocabs.config.ProjectConfig;
import in.myvocabs.constants.Constants;
import in.myvocabs.fragments.dialog.AppUpdateDialogFragment;
import in.myvocabs.model.version.Data;

import static android.support.v4.app.NotificationCompat.VISIBILITY_PUBLIC;

/**
 * Created by kailash karki on 10/20/2014.
 */
public class AppUtils {

    public static void showToast(String iMessage) {
        if (ProjectConfig.SHOULD_SHOW_TOAST) {
            Toast.makeText(WordOfTheDayApplication.getAppContext(), iMessage, Toast.LENGTH_LONG).show();
        }
    }

    public static void hideKeyboard(Activity activity) {
        try {
            InputMethodManager inputManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) {
            // Ignore exceptions if any
            Log.e("KeyBoardUtil", e.toString(), e);
        }
    }


    /**
     * conversion of string timestamp into date
     *
     * @param timeStamp in string
     * @return return date format
     */
    public static String convertTimeStampInToDate(String timeStamp) {
        Timestamp stamp = new Timestamp(Validator.validLong(timeStamp));
        Date date = new Date(stamp.getTime());
        @SuppressLint("SimpleDateFormat") SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Constants.SIMPLE_DATE_REGEX);
        return simpleDateFormat.format(date).toUpperCase();

    }

    public static boolean isNetworkAvailable(final Context context) {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }

    public static void checkNetwork(Context context) {
        if (AppUtils.isNetworkAvailable(context)) {

        } else {
            AppUtils.showToast("Network not available! Check your internet connection");
        }
    }

    public static void setListAnimation(Context context, RecyclerView recyclerView) {
        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down);
        recyclerView.setLayoutAnimation(animation);
    }


    public static Date formatDate(String input) {
        try {
            String dd[] = input.split("T");
            SimpleDateFormat inputDf = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat outputDf = new SimpleDateFormat("dd-MM-yyyy");
            Date date = inputDf.parse(dd[0]);
            return outputDf.parse(outputDf.format(date));
        } catch (ParseException e) {
            return null;
        }
    }

    public static String convertDate(Date date) {
        try {
            SimpleDateFormat outputDf = new SimpleDateFormat("dd-MM-yyyy");
            return outputDf.format(date);
        } catch (Exception e) {
            return null;
        }
    }

    public static void shareWord(Context context, String word, String meaning) {
        try {
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("text/plain");
            String sAux = "What does \"" + word + "\" mean? \n\nLearn a new word daily. ";
            sAux = sAux + "\n\n https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID + " \n\n";
            i.putExtra(Intent.EXTRA_TEXT, sAux);
            context.startActivity(Intent.createChooser(i, "English Updates share"));
        } catch (Exception e) {
            //e.toString();
        }
    }

    public static void shareQuote(Context context, String quote, String author) {
        try {
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("text/plain");
            String sAux = "\"" + quote + "\" \n\n said " + author;
            sAux = sAux + "\n\n https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID + " \n\n";
            i.putExtra(Intent.EXTRA_TEXT, sAux);
            context.startActivity(Intent.createChooser(i, "English Updates share"));
        } catch (Exception e) {
        }
    }

    public static void shareQuote(Context context) {
        try {
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("text/plain");
            String sAux = "Download App to learn a new word daily.";
            sAux = sAux + "\n\n https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID + " \n\n";
            i.putExtra(Intent.EXTRA_TEXT, sAux);
            context.startActivity(Intent.createChooser(i, "English Updates share"));
        } catch (Exception e) {
        }
    }

    public static void setLikeBtnAnimation(Context context, View view) {
        // Button button = (Button)findViewById(R.id.button);
        final Animation myAnim = AnimationUtils.loadAnimation(context, R.anim.like_btn);

        // Use bounce interpolator with amplitude 0.2 and frequency 20
        MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
        myAnim.setInterpolator(interpolator);

        view.startAnimation(myAnim);
    }

    public static void showAppUpdateDialog(Data appData, Activity activity) {
        try {

            if (null != appData.getCritclVcode() && null != appData.getVcode() && null != appData.getVname()) {
                int newVerCode = appData.getVcode();
                int criticalCode = appData.getCritclVcode();
                int currentCode = BuildConfig.VERSION_CODE;
                boolean isCritical = newVerCode == criticalCode;
                if (currentCode < newVerCode) {
                    if (isCritical || currentCode < criticalCode) {
                        AppUpdateDialogFragment dialogFragment = new AppUpdateDialogFragment();
                        Bundle bundle = new Bundle();
                        bundle.putParcelable("data", appData);
                        dialogFragment.setArguments(bundle);
                        dialogFragment.show(activity.getFragmentManager(), activity.getString(R.string.app_ver_tag));
                    } else {
                        int oldVerCode = Pref.readInteger(activity, Constants.IS_UPDATE_SHOWN);
                        if (oldVerCode < newVerCode) {
                            AppUpdateDialogFragment dialogFragment = new AppUpdateDialogFragment();
                            Bundle bundle = new Bundle();
                            bundle.putParcelable("data", appData);
                            dialogFragment.setArguments(bundle);
                            dialogFragment.show(activity.getFragmentManager(), activity.getString(R.string.app_ver_tag));
                            Pref.writeInteger(activity, Constants.IS_UPDATE_SHOWN, newVerCode);
                        }
                    }
                }
            }

        } catch (Exception e) {
            AppLog.e("BASIC UTILS-->", "" + e.getMessage());
        }
    }


    public static void showWordNotification(Context context, String title, String text) {
        Intent intent = new Intent(context, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);
        final long[] DEFAULT_VIBRATE_PATTERN = {0, 250, 250, 250};

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, Constants.NOTIFICATIONS_CHANNEL_ID_WORD)
                .setSmallIcon(R.drawable.logo)
                .setContentTitle("Word of The Day is  \"" + title + "\"")
                .setContentText("Meaning : " + text.substring(0, text.length() / 2) + "...")
                //   .setStyle(new NotificationCompat.BigTextStyle().bigText("Meaning : " + text.substring(0, text.length() / 2) + "..."))
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setVisibility(VISIBILITY_PUBLIC)
                .setAutoCancel(true)
                .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                .setVibrate(DEFAULT_VIBRATE_PATTERN)
                .setContentIntent(pendingIntent);
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
        notificationManager.notify(1, mBuilder.build());
    }

    public static void showQuoteNotification(Context context, String quote, String author) {
        Intent intent = new Intent(context, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);
        final long[] DEFAULT_VIBRATE_PATTERN = {0, 250, 250, 250};

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, Constants.NOTIFICATIONS_CHANNEL_ID_WORD)
                .setSmallIcon(R.drawable.logo)
                .setContentTitle("Quote of The Day")
                .setContentText(quote + "\n -by " + author)
                //   .setStyle(new NotificationCompat.BigTextStyle().bigText("Meaning : " + text.substring(0, text.length() / 2) + "..."))
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setVisibility(VISIBILITY_PUBLIC)
                .setAutoCancel(true)
                .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                .setVibrate(DEFAULT_VIBRATE_PATTERN)
                .setContentIntent(pendingIntent);
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
        notificationManager.notify(2, mBuilder.build());
    }
}
