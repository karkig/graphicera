package in.myvocabs.util;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import java.util.Calendar;

import in.myvocabs.services.NotificationServiceService;

public class AppNotificationManager {

    public static final String WORD_ACTION = "WORD";
    public static final String QUOTE_ACTION = "QUOTE";

    public static void setAlarm(Context context) {

        Calendar calendar1 = Calendar.getInstance();
        calendar1.set(Calendar.HOUR_OF_DAY, 16); // For 4 PM
        calendar1.set(Calendar.MINUTE, 38);
        calendar1.set(Calendar.SECOND, 5);
        PendingIntent pi = PendingIntent.getService(context, 0,
                new Intent(context, NotificationServiceService.class).setAction(WORD_ACTION), PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager am1 = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        am1.setRepeating(AlarmManager.RTC_WAKEUP, calendar1.getTimeInMillis(),
                AlarmManager.INTERVAL_DAY, pi);

        Calendar calendar2 = Calendar.getInstance();
        calendar2.set(Calendar.HOUR_OF_DAY, 11); // For 11 AM
        calendar2.set(Calendar.MINUTE, 39);
        calendar2.set(Calendar.SECOND, 5);
        PendingIntent pendingIntentQuote = PendingIntent.getService(context, 1,
                new Intent(context, NotificationServiceService.class).setAction(QUOTE_ACTION), PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager am2 = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        am2.setRepeating(AlarmManager.RTC_WAKEUP, calendar2.getTimeInMillis(),
                AlarmManager.INTERVAL_DAY, pendingIntentQuote);
    }
}