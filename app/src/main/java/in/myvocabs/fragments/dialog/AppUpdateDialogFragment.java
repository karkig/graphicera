package in.myvocabs.fragments.dialog;

import android.app.DialogFragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jakewharton.rxbinding.view.RxView;

import in.myvocabs.BuildConfig;
import in.myvocabs.R;
import in.myvocabs.model.version.Data;
import in.myvocabs.model.version.VersionResponse;

/**
 * Created by kailash on 3/22/2016.
 */
public class AppUpdateDialogFragment extends DialogFragment {

    private TextView update;
    private boolean isAForceUpdate = false;
    private TextView dialogMsz;
    private Data data;
    private TextView currentVer;
    TextView skip;

    public AppUpdateDialogFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.app_update_dialog_fragment, container,
                false);
        Bundle bundle = getArguments();
        data = bundle.getParcelable("data");
        update = rootView.findViewById(R.id.update);
        dialogMsz = rootView.findViewById(R.id.dialogMsz);
        currentVer = rootView.findViewById(R.id.currentVer);
        skip = rootView.findViewById(R.id.skip);
        setUpDialog(data);
        return rootView;
    }

    private void setUpDialog(Data response) {


        int newVerCode = response.getVcode();
        int criticalVerCode = response.getCritclVcode();
        int currentCode = BuildConfig.VERSION_CODE;
        currentVer.setText("Your current version is " + BuildConfig.VERSION_NAME);

        if (criticalVerCode == newVerCode || currentCode < criticalVerCode) {
            getDialog().setCancelable(false);
            getDialog().setCanceledOnTouchOutside(false);
            skip.setVisibility(View.GONE);
        } else {
            skip.setVisibility(View.VISIBLE);
        }
        RxView.clicks(skip).subscribe(aVoid -> dismiss());
        RxView.clicks(update).subscribe(aVoid -> startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse
                ("market://details?id=" + BuildConfig.APPLICATION_ID))));
        dialogMsz.setText("New app version in available. Please update your app from playstore. ");

    }

    @Override
    public void onResume() {
        super.onResume();
        getDialog().setOnKeyListener((dialog, keyCode, event) -> {
            if ((keyCode == android.view.KeyEvent.KEYCODE_BACK)) {
                return true; // pretend we've processed it
            } else
                return true; // pass on to be processed as normal
        });
    }

}
